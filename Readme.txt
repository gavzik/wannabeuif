==============================================================================
Wannabe Universal Import Fixer (WannabeUIF) v1.1 by SmilingWolf

Use this tool to fix Import Elimination, Directly Imports and
Shuffled, Disordered, Scattered Imports (Just for 64 bit processes).

So you can use this tool to change IAT Base Address and Sorting IATs 
in New (other) Address.

Tested on:
lARP64 Pro

Notes:
======
This tool is an Import Fixer (not Import Rebuilder ImpRec etc) and just works
with the memory of the target process. Don't ask me how to use this tool... If you
can not use this simple tool please DRAG IT TO THE RECYCLE BIN ok?

Always first use WannabeUIF then dump target process.

WannabeUIF can fix actual APIs, don't use it for fixing Emulated/Redirected APIs to
protector's stub. You must use WannabeUIF after fixing Magic IAT jump 
(or use any methods) to convert Emulated/Redirected APIs to actual APIs.

Samples:
lARP64 Pro: Simple + Advanced Import Redirection

Supported instructions:
=======================
call qword ptr ds:[RIP+disp]
jmp  qword ptr ds:[RIP+disp]
mov  qword ptr ds:[RIP+disp], reg
mov  reg, qword ptr ds:[RIP+disp]
lea  reg, qword ptr ds:[RIP+disp]

call qword ptr ds:[MEM]
jmp  qword ptr ds:[MEM]
mov  qword ptr ds:[MEM], reg
mov  reg, qword ptr ds:[MEM]
lea  reg, qword ptr ds:[MEM]

On x86_64 call/jmp qword ptr ds:[RIP+disp]/qword ptr ds:[MEM] instructions can be prefixed with superflous 0x4X bytes.
This situation is handled correctly.

How to use :
============
1.fill <PID (hex)> with target Process ID.

2.fill <Start VA> with start address (Virtual Address) of code that you want to fix it.

3.fill <End VA> with End address (Virtual Address) of code that you want to fix it.

4.fill <New VA> with address (Virtual Address) of Empty or unused area 
  (in Code section or Data section or any...) that IAT will repair to it.

History:
========


v1.1 Public (2015.01.22):
=========================
Fixed the handle/memory leaks happening when
the procedure is interrupted because of an error
A few more sanity checks on the input values
Added Fast Speed mode
Added the ability to save the log

v1.0 Public (2015.01.19):
=========================
First public release.
Rewritten a part of the code to use C++ maps. This resulted in a nice
speed improvement, with automatic sorting and dupes removal.
Removed dependency upon XEDParse with a lot of positive consequences
(code cleanup) and a nice speed gain

v1.0 Private (2014.12.28):
==========================
For personal use.
Written in pure C + WinAPI using Capstone v3.0 as disassembler
and XEDParse as assembler.

FAQ:
====
Q: Why have you coded <insert function XY here> like that?
A: Because it looked like a good idea (or at least a decent one, or a fast one to implement)
   when I first thought about it. If you can code it better go ahead; if you want to rewrite
   the whole tool go ahead (and share the result, please :)). Besides, I have always coded
   in pure C and the code reflects this (you've got to thank Mr. eXoDia for the initial C++ code),
   so any help in porting this thing to C++ would really help.

Q: Your tool/code sucks.
A: This is not a question, it's a statement, and a pretty pointless one. Tell me why and,
   if you can, submit a correction or at least some ideas to improve the code, please.

Q: Is there room for improvement?
A: Sure! I have compiled a little file (Tests.txt) with the speeds of some old implementations and ideas
   plus the current one. Speeds have already improved a bit since the first implementation, for sure there
   are ways to make it go even faster :)

Q: Under which terms is this licensed?
A: Anything that lets you enjoy the tool and fiddle with the source as you please (aka I still don't know).
   The reversing world needs more open source tools and the 64bit universe lacks all the nice accessories
   we have on 32bit, I'm just trying to fill the gaps, whatever else is not that important.


SmilingWolf
https://forum.tuts4you.com/user/71368-smilingwolf/

==============================================================================
