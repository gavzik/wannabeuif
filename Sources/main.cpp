#include <windows.h>
#include <commctrl.h>
#include <stdio.h>
#include <DbgHelp.h>
#include <tlhelp32.h>
#include <tchar.h>

#include <string>
#include <iostream>
#include <map>
#include <vector>

#include "resource.h"
#include "include\capstone.h"

#define fhex "%.16llX"

using namespace std;

void SimpleLog(HWND hWnd, const char* Message, ...);

HINSTANCE hInst;

typedef std::pair<uint64_t, uint64_t> Range;

struct RangeCompare
{
    bool operator()(const Range & a, const Range & b) const //a before b?
    {
        return a.second < b.first;
    }
};

struct InstrucionInfo
{
    uint64_t address;
    uint16_t size;
};

struct Import //structure that describes an import
{
    uint64_t addr;
    string name;
    vector<InstrucionInfo> references;

public:
    Import(uint64_t addr, string name)
    {
        this->addr = addr;
        this->name = name;
    }
};
typedef map<uint64_t, Import> ImportMap;

struct Module
{
    uint64_t base; //module base
    string name;
    ImportMap imports;

public:
    Module(uint64_t base, string name)
    {
        this->base = base;
        this->name = name;
    }
};
typedef map<Range, Module, RangeCompare> ModuleMap;

void printImport(HWND hwndDlg, const Import & imp)
{
    for(vector<InstrucionInfo>::const_iterator i = imp.references.begin(); i != imp.references.end(); ++i)
        SimpleLog(hwndDlg, fhex": "fhex": %s\r\n", i->address, imp.addr, imp.name.c_str());
}

void printModule(HWND hwndDlg, const Module & mod)
{
    SimpleLog(hwndDlg, "name: %s\r\n", mod.name.c_str());
    SimpleLog(hwndDlg, "base: 0x"fhex"\r\n", mod.base);
    for(ImportMap::const_iterator i = mod.imports.begin(); i != mod.imports.end(); ++i)
        printImport(hwndDlg, i->second);
    SimpleLog(hwndDlg, "\r\n");
}

ModuleMap modules;

void addReference(Import & imp, InstrucionInfo refaddr)
{
    imp.references.push_back(refaddr);
}

void addReferenceToImport(HWND hwndDlg, uint64_t importaddr, InstrucionInfo refaddr)
{
    ModuleMap::iterator modfound = modules.find(Range(importaddr, importaddr));
    if(modfound == modules.end())
    {
        SimpleLog(hwndDlg, "Error! Module containing import at addr 0x"fhex" not found!", importaddr);
        return;
    }

    ImportMap::iterator impfound = modfound->second.imports.find(importaddr);
    if(impfound == modfound->second.imports.end())
    {
        SimpleLog(hwndDlg, "Error! Import at addr 0x"fhex" not found!", importaddr);
        return;
    }
    addReference(impfound->second, refaddr);
}

void addImport(Module & mod, const Import & imp)
{
    mod.imports.insert(make_pair(imp.addr, imp));
}

void addImportToModule(HWND hwndDlg, uint64_t modbase, const Import & imp)
{
    ModuleMap::iterator found = modules.find(Range(modbase, modbase));
    if(found == modules.end())
    {
        SimpleLog(hwndDlg, "Error! Module 0x"fhex" not found!", modbase);
        return;
    }
    addImport(found->second, imp);
}

// http://stackoverflow.com/a/2221157
bool EnableDebugPriv()
{
    HANDLE              hToken;
    LUID                SeDebugNameValue;
    TOKEN_PRIVILEGES    TokenPrivileges;

    if(OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken))
    {
        if(LookupPrivilegeValue(NULL, SE_DEBUG_NAME, &SeDebugNameValue))
        {
            TokenPrivileges.PrivilegeCount              = 1;
            TokenPrivileges.Privileges[0].Luid          = SeDebugNameValue;
            TokenPrivileges.Privileges[0].Attributes    = SE_PRIVILEGE_ENABLED;

            if(AdjustTokenPrivileges(hToken, FALSE, &TokenPrivileges, sizeof(TOKEN_PRIVILEGES), NULL, NULL))
            {
                CloseHandle(hToken);
            }
            else
            {
                CloseHandle(hToken);
                MessageBox(NULL, "Couldn't adjust token privileges!", "Error", MB_ICONERROR);
                return FALSE;
            }
        }
        else
        {
            CloseHandle(hToken);
            MessageBox(NULL, "Couldn't look up privilege value!", "Error", MB_ICONERROR);
            return FALSE;
        }
    }
    else
    {
        MessageBox(NULL, "Couldn't open process token!", "Error", MB_ICONERROR);
        return FALSE;
    }
    return TRUE;
}

void SimpleLog(HWND hwndDlg, const char* Message, ...)
{
    va_list args;
    va_start(args, Message);
    LPVOID msg = VirtualAlloc(NULL, 0x1000, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
    HWND hEdit = GetDlgItem(hwndDlg, EDT_LOG);
    int ndx = GetWindowTextLength(hEdit);

    vsprintf_s((char*)msg, 0x1000, Message, args);
    SetFocus(hEdit);
    SendMessage(hEdit, EM_SETSEL, (WPARAM)ndx, (LPARAM)ndx);
    SendMessage(hEdit, EM_REPLACESEL, 0, (LPARAM)msg);
    VirtualFree(msg, 0, MEM_RELEASE);
}

bool LoadModuleList(DWORD dwPID)
{
    HANDLE hModuleSnap = INVALID_HANDLE_VALUE;
    MODULEENTRY32 me32;

    //  Take a snapshot of all modules in the specified process.
    hModuleSnap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE | TH32CS_SNAPMODULE32, dwPID);
    if(hModuleSnap == INVALID_HANDLE_VALUE)
    {
        return FALSE;
    }

    //  Set the size of the structure before using it.
    me32.dwSize = sizeof(MODULEENTRY32);

    //  Retrieve information about the first module,
    //  and exit if unsuccessful
    if(!Module32First(hModuleSnap, &me32))
    {
        CloseHandle(hModuleSnap);     // Must clean up the snapshot object!
        return FALSE;
    }

    //  Now walk the module list of the process,
    //  and fill the ModuleMap with informations about each module
    do
    {
        string ModuleName(me32.szModule);
        Module CurrMod((uint64_t) me32.modBaseAddr, ModuleName);
        modules.insert(make_pair(Range(CurrMod.base, CurrMod.base + (uint64_t) me32.modBaseSize - 1), CurrMod));
    }
    while(Module32Next(hModuleSnap, &me32));

    //  Do not forget to clean up the snapshot object.
    CloseHandle(hModuleSnap);

    return TRUE;
}

BOOL CALLBACK DlgMain(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch(uMsg)
    {
    case WM_INITDIALOG:
    {
        SendDlgItemMessageA(hwndDlg, EDT_STARTVA, EM_SETLIMITTEXT, 16, 0);
        SendDlgItemMessageA(hwndDlg, EDT_ENDVA, EM_SETLIMITTEXT, 16, 0);
        SendDlgItemMessageA(hwndDlg, EDT_NEWVA, EM_SETLIMITTEXT, 16, 0);
        SendDlgItemMessageA(hwndDlg, CHK_FASTSPEED, BM_SETCHECK, BST_CHECKED, 0);
        SendDlgItemMessageA(hwndDlg, EDT_LOG, EM_SETLIMITTEXT, 0, 0);
        SimpleLog(hwndDlg, "Wannabe Universal Import Fixer v1.1 by SmilingWolf\r\n");
        SimpleLog(hwndDlg, "First version:   2014.12.28\r\n");
        SimpleLog(hwndDlg, "Current version: 2015.01.22\r\n\r\n");
    }
    return TRUE;

    case WM_CLOSE:
    {
        EndDialog(hwndDlg, 0);
    }
    return TRUE;

    case WM_COMMAND:
    {
        switch(LOWORD(wParam))
        {
        case BTN_MAGIC:
        {
            char _PID[9] = { 0x00 };
            char _StartVA[17] = { 0x00 };
            char _EndVA[17] = { 0x00 };
            char _NewVA[17] = { 0x00 };
            BOOL bIsWow64 = FALSE;
            BOOL FastSpeed;
            DWORD PID = 0;
            uint64_t StartVA = 0;
            uint64_t EndVA = 0;
            uint64_t NewVA = 0;
            uint64_t ImportAddr = 0;
            uint64_t NewImportVA = 0;
            int64_t DestAddr = 0;
            int32_t Offset = 0;
            size_t CodeSize = 0;
            HANDLE hProcess;
            DWORD CodeOldProtect;
            DWORD NewVAOldProtect;
            char SymbolBuffer[sizeof(SYMBOL_INFO) + MAX_SYM_NAME * sizeof(TCHAR)];
            PSYMBOL_INFO pSymbol = (PSYMBOL_INFO)SymbolBuffer;
            const uint8_t* code;
            csh csHandle;
            cs_err err;
            cs_insn* insn;
            cs_detail* detail;
            DWORD starttime;
            DWORD endtime;

            starttime = GetTickCount();

            SimpleLog(hwndDlg, "Initializing...\r\n");

            if(!GetDlgItemText(hwndDlg, EDT_PID, _PID, 9))
            {
                SimpleLog(hwndDlg, "Error! No Process ID given!\r\n");
                return TRUE;
            }
            sscanf_s(_PID, "%16llX", &PID);
            if(!GetDlgItemText(hwndDlg, EDT_STARTVA, _StartVA, 17))
            {
                SimpleLog(hwndDlg, "Error! No Process Start VA given!\r\n");
                return TRUE;
            }
            sscanf_s(_StartVA, "%16llX", &StartVA);
            if(!GetDlgItemText(hwndDlg, EDT_ENDVA, _EndVA, 17))
            {
                SimpleLog(hwndDlg, "Error! No Process End VA given!\r\n");
                return TRUE;
            }
            sscanf_s(_EndVA, "%16llX", &EndVA);
            if(!GetDlgItemText(hwndDlg, EDT_NEWVA, _NewVA, 17))
            {
                SimpleLog(hwndDlg, "Error! No New VA given!\r\n");
                return TRUE;
            }
            sscanf_s(_NewVA, "%16llX", &NewVA);

            FastSpeed = SendDlgItemMessage(hwndDlg, CHK_FASTSPEED, BM_GETCHECK, 0, 0);

            if(StartVA >= EndVA)
            {
                SimpleLog(hwndDlg, "Error! Start VA is >= End VA!\r\n");
                return TRUE;
            }
            else
            {
                CodeSize = EndVA - StartVA;
                SimpleLog(hwndDlg, "Info! Code size is %.16llX\r\n", CodeSize);
            }

            if(_abs64(NewVA - StartVA) > MAXINT32 || _abs64(NewVA - EndVA) > MAXINT32)
            {
                SimpleLog(hwndDlg, "Error! Displacement is too big!\r\n");
                return TRUE;
            }

            hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, PID);
            if(!hProcess)
            {
                SimpleLog(hwndDlg, "Error! Process ID is invalid or process is protected!\r\n");
                return TRUE;
            }

            IsWow64Process(hProcess, &bIsWow64);
            if(bIsWow64)
            {
                SimpleLog(hwndDlg, "Error! Process %X is a 32bit process!\r\n", PID);
                CloseHandle(hProcess);
                return TRUE;
            }

            if(!LoadModuleList(PID))
            {
                SimpleLog(hwndDlg, "Error! Couldn't succesfully enum the process' modules!\r\n");
                CloseHandle(hProcess);
                return TRUE;
            }

            // Remove the current module from the modules list
            ModuleMap::iterator currmodfound = modules.find(Range(StartVA, EndVA));
            if(currmodfound != modules.end())
                modules.erase(currmodfound);
            // for(ModuleMap::const_iterator i = modules.begin(); i != modules.end(); ++i)
            //     printModule(hwndDlg, i->second);

            if(!FastSpeed)
            {
                SymSetOptions(SYMOPT_UNDNAME | SYMOPT_DEFERRED_LOADS);
                SymInitialize(hProcess, NULL, TRUE);
                pSymbol->SizeOfStruct = sizeof(SYMBOL_INFO);
                pSymbol->MaxNameLen = MAX_SYM_NAME;
            }

            err = cs_open(CS_ARCH_X86, CS_MODE_64, &csHandle);
            if(err)
            {
                SimpleLog(hwndDlg, "Error! Capstone could not initialize!\r\n");
                modules.clear();
                SymCleanup(hProcess);
                CloseHandle(hProcess);
                return TRUE;
            }

            if(!VirtualProtectEx(hProcess, (LPVOID)StartVA, CodeSize, PAGE_EXECUTE_READWRITE, &CodeOldProtect))
            {
                SimpleLog(hwndDlg, "Error! Couldn't change code page's permissions!\r\n");
                modules.clear();
                cs_close(&csHandle);
                SymCleanup(hProcess);
                CloseHandle(hProcess);
                return TRUE;
            }

            code = (unsigned char*)VirtualAlloc(NULL, CodeSize, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
            if(!ReadProcessMemory(hProcess, (LPCVOID)StartVA, (LPVOID)code, CodeSize, NULL))
            {
                SimpleLog(hwndDlg, "Error! Couldn't read the memory of the process!\r\n");
                modules.clear();
                cs_close(&csHandle);
                SymCleanup(hProcess);
                VirtualFree((LPVOID)code, 0, MEM_RELEASE);
                CloseHandle(hProcess);
                return TRUE;
            }

            SimpleLog(hwndDlg, "Initialized succesfully!\r\n\r\n");

            cs_option(csHandle, CS_OPT_SKIPDATA, CS_OPT_ON);
            cs_option(csHandle, CS_OPT_DETAIL, CS_OPT_ON);
            insn = cs_malloc(csHandle);

            int mightfix = 0;
            int NDiffImports = 0;
            int ImportIndex = 0;
            int ModIndex = 0;
            int mod;
            int rm;
            bool is_rip_relative;
            const uint8_t* Bakcode = code;
            uint64_t BakCodeSize = CodeSize, BakStartVA = StartVA;

            while(cs_disasm_iter(csHandle, &code, &CodeSize, &StartVA, insn))
            {
                detail = insn->detail;
                if(insn->id == X86_INS_CALL || insn->id == X86_INS_JMP || insn->id == X86_INS_MOV || insn->id == X86_INS_LEA)
                {
                    mod = (detail->x86.modrm & 0xc0) >> 6;
                    rm = (detail->x86.modrm & 0x07) >> 0;
                    is_rip_relative = (mod == 0 && rm == 5);
                    if(is_rip_relative || ((detail->x86.operands[0].type == X86_OP_MEM || detail->x86.operands[1].type == X86_OP_MEM) && detail->x86.sib != 0))
                    {
                        // SimpleLog(hwndDlg, "0x"fhex":\t%s\t%s\r\n", insn->address, insn->mnemonic, insn->op_str);
                        // SimpleLog(hwndDlg, "Disp: "fhex"\r\n", detail->x86.disp);
                        if(is_rip_relative)   // call/jmp qword ptr ds:[RIP+disp]
                            DestAddr = insn->address + insn->size + detail->x86.disp;
                        else                  // call/jmp qword ptr ds:[MEM]
                            DestAddr = detail->x86.disp;

                        ImportAddr = 0;
                        ReadProcessMemory(hProcess, (LPCVOID)DestAddr, &ImportAddr, sizeof(ImportAddr), NULL);
                        ModuleMap::iterator found = modules.find(Range(ImportAddr, ImportAddr));
                        if(found != modules.end())
                        {
                            ImportMap::iterator impfound = found->second.imports.find(ImportAddr);
                            if(impfound == found->second.imports.end())
                            {
                                if(!FastSpeed)
                                {
                                    SymFromAddr(hProcess, ImportAddr, NULL, pSymbol);
                                    addImportToModule(hwndDlg, ImportAddr, Import(ImportAddr, pSymbol->Name));
                                }
                                else
                                    addImportToModule(hwndDlg, ImportAddr, Import(ImportAddr, ""));
                                NDiffImports++;
                            }

                            InstrucionInfo CurrentInstruction;
                            CurrentInstruction.address = insn->address;
                            CurrentInstruction.size = insn->size;
                            addReferenceToImport(hwndDlg, ImportAddr, CurrentInstruction);
                        }
                    }
                }
            }

            //for(ModuleMap::const_iterator i = modules.begin(); i != modules.end(); ++i)
            //    printModule(hwndDlg, i->second);

            if(!NDiffImports)
            {
                SimpleLog(hwndDlg, "Error! No imports to fix!\r\n");
                modules.clear();
                cs_free(insn, 1);
                cs_close(&csHandle);
                SymCleanup(hProcess);
                VirtualFree((LPVOID)code, 0, MEM_RELEASE);
                CloseHandle(hProcess);
                return TRUE;
            }

            if(!VirtualProtectEx(hProcess, (LPVOID)NewVA, NDiffImports * sizeof(ImportAddr), PAGE_EXECUTE_READWRITE, &NewVAOldProtect))
            {
                SimpleLog(hwndDlg, "Error! Couldn't change the new imports page's permissions!\r\n");
                modules.clear();
                VirtualProtectEx(hProcess, (LPVOID)BakStartVA, BakCodeSize, CodeOldProtect, &CodeOldProtect);
                cs_free(insn, 1);
                cs_close(&csHandle);
                SymCleanup(hProcess);
                VirtualFree((LPVOID)code, 0, MEM_RELEASE);
                CloseHandle(hProcess);
                return TRUE;
            }

            for(ModuleMap::const_iterator modi = modules.begin(); modi != modules.end(); ++modi)
            {
                if(!modi->second.imports.empty())
                {
                    if(!FastSpeed)
                    {
                        SimpleLog(hwndDlg, "%s\r\n"fhex"\r\n", modi->second.name.c_str(), modi->second.base);
                    }
                }
                for(ImportMap::const_iterator impi = modi->second.imports.begin(); impi != modi->second.imports.end(); ++impi)
                {
                    NewImportVA = NewVA + ImportIndex * sizeof(ImportAddr) + ModIndex * sizeof(ImportAddr);
                    ImportAddr = impi->second.addr;
                    if(!WriteProcessMemory(hProcess, (LPVOID)NewImportVA, &ImportAddr, sizeof(ImportAddr), NULL))
                    {
                        SimpleLog(hwndDlg, "Error! Couldn't write to the new VA!\r\n");
                        modules.clear();
                        VirtualProtectEx(hProcess, (LPVOID)BakStartVA, BakCodeSize, CodeOldProtect, &CodeOldProtect);
                        VirtualProtectEx(hProcess, (LPVOID)NewVA, NDiffImports * sizeof(ImportAddr), NewVAOldProtect, &NewVAOldProtect);
                        cs_free(insn, 1);
                        cs_close(&csHandle);
                        SymCleanup(hProcess);
                        VirtualFree((LPVOID)Bakcode, 0, MEM_RELEASE);
                        CloseHandle(hProcess);
                        return TRUE;
                    }
                    for(vector<InstrucionInfo>::const_iterator refi = impi->second.references.begin(); refi != impi->second.references.end(); ++refi)
                    {
                        if(!FastSpeed)
                        {
                            SimpleLog(hwndDlg, fhex": "fhex": %s\r\n", refi->address, impi->second.addr, impi->second.name.c_str());
                        }
                        uint64_t address = refi->address;
                        uint16_t size = refi->size;

                        Offset = (int32_t)(NewImportVA - (address + size));

                        WriteProcessMemory(hProcess, (LPVOID)(address + size - sizeof(Offset)), &Offset, sizeof(Offset), NULL);
                        mightfix++;
                    }
                    ImportIndex++;
                }
                if(!modi->second.imports.empty())
                {
                    ModIndex++;
                    if(!FastSpeed)
                    {
                        SimpleLog(hwndDlg, "\r\n");
                    }
                }
            }

            endtime = GetTickCount();
            SimpleLog(hwndDlg, "Time: %d ticks!\r\n", endtime - starttime);
            SimpleLog(hwndDlg, "Fixed %d instructions!\r\n", mightfix);
            SimpleLog(hwndDlg, "Found %d different imports!\r\n", NDiffImports);

            modules.clear();
            VirtualProtectEx(hProcess, (LPVOID)BakStartVA, BakCodeSize, CodeOldProtect, &CodeOldProtect);
            VirtualProtectEx(hProcess, (LPVOID)NewVA, NDiffImports * sizeof(ImportAddr), NewVAOldProtect, &NewVAOldProtect);
            cs_free(insn, 1);
            cs_close(&csHandle);
            SymCleanup(hProcess);
            VirtualFree((LPVOID)Bakcode, 0, MEM_RELEASE);
            CloseHandle(hProcess);
        }
        return TRUE;

        case BTN_EXIT:
        {
            SendMessageA(hwndDlg, WM_CLOSE, 0, 0);
        }
        return TRUE;

        case BTN_SAVELOG:
        {
            int LogLength;
            int MemLength;
            uint8_t* LogText;
            HWND LogHwnd;
            OPENFILENAME FileName = { 0x00 };
            HANDLE LogFile;
            DWORD lpNumberOfBytesWritten;
            char FileString[MAX_PATH] = "WannabeUIF-log.txt\0";

            FileName.lpstrFilter = "*.txt\0*.txt\0*.*\0*.*\0\0";
            FileName.lpstrFile = FileString;
            FileName.nMaxFile = MAX_PATH;
            FileName.lpstrDefExt = "txt\0";
            FileName.lStructSize = sizeof(OPENFILENAME);

            if(GetSaveFileName(&FileName))
            {
                LogFile = CreateFile(FileName.lpstrFile, GENERIC_WRITE, NULL, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
                if(!LogFile)
                {
                    SimpleLog(hwndDlg, "Error! Cannot create file %s!", FileName.lpstrFile);
                    return TRUE;
                }
                LogHwnd = GetDlgItem(hwndDlg, EDT_LOG);
                MemLength = GetWindowTextLength(LogHwnd);
                LogText = (unsigned char*)VirtualAlloc(NULL, MemLength + 1, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
                LogLength = GetWindowText(LogHwnd, (LPSTR)LogText, MemLength);
                if(!WriteFile(LogFile, LogText, LogLength, &lpNumberOfBytesWritten , NULL))
                {
                    SimpleLog(hwndDlg, "Error! Cannot write in the specified file!");
                }
                VirtualFree((LPVOID)LogText, 0, MEM_RELEASE);
                CloseHandle(LogFile);
            }
        }
        return TRUE;

        }
    }
    return TRUE;
    }
    return FALSE;
}

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
    hInst = hInstance;
    if(!EnableDebugPriv())
    {
        if(MessageBox(NULL, "Error! Could not enable debug privilege!\r\nContinue anyway?", "Error", MB_YESNO | MB_ICONERROR) == IDNO)
            return FALSE;
    }
    InitCommonControls();
    return DialogBox(hInst, MAKEINTRESOURCE(DLG_MAIN), NULL, (DLGPROC)DlgMain);
}